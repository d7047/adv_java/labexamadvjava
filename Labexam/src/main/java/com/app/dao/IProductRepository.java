package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.Product;

public interface IProductRepository extends JpaRepository<Product, Long> {
@Query("select p from product p join p.user where p.user.id=?1")
 List<Product>findByUserId(Long userId);

}
