package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User  extends BaseEntity{
	@Column(unique=true)
	@NotNull
private String name;
@Column(nullable=false)
private String password;
@Enumerated(EnumType.STRING)
	private Role role;

}
