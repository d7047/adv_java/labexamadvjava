package com.app.service;

import java.util.Optional;

import com.app.entities.User;

public interface IUserService {
User findByNameAndPassword(String name,String password);
User findUserById(long userId);
}
