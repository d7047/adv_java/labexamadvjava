<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Home</title>
</head>
<body>
<h1 align="center">${requestScope.user}</h1>
<table style="margin: auto">
<tr>
<th>P_Id</th>
<th>P_Name</th>
<th>P_Desc</th>
<th>P_Qnty</th>
<th>P_Price</th>
</tr>
<c:forEach var="product" items="${requestScope.products}">
<td>${product.id}</td>
<td>${product.name}</td>
<td>${product.desc}</td>
<td>${product.qnty}</td>
<td>${product.price}</td>
</c:forEach>
</table>
<a href="<spring:url value="/user/add"/>">Add new Product</a>
</body>
</html>