<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://www.springframework.org/tags"  prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Index</title>
</head>
<body>
<h1 align="center">Welcome</h1>
<spring:url var="url"  value="/login" />
<h1 align="center"><a href="${url}">Login</a></h1>
</body>
</html>